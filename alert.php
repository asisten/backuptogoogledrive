<?php
// mail functiom, using mailgun get information from here:  https://app.mailgun.com/app/domains/yoursub.domain.com

function send_alert() {

		$sender_name = 'Backup System';
		$sender_email = MAILGUN_MAIL_SENDER;	
	    $to_name = 'Site Owner';
        $to_email = MY_EMAIL;	
        if(!filter_var($sender_email, FILTER_VALIDATE_EMAIL)){ 
            return false;
        }

        if(!filter_var($to_email, FILTER_VALIDATE_EMAIL)){ 
            return false;
        }
        
        $subject = $mysite. ' Have Reach '.MAX_BACKUP_ALERT.' Backup Times On Google Drive';
        $message = 'You have reach max backup for '.MAX_BACKUP_ALERT.' times. Please considered to check your Google Drive storage quota. Your Google Drive Account: '.GMAIL_DRIVE;

 $array_data = array(
		'from'=>$sender_name.'<'.$sender_email.'>',
		'to'=>$to_name.'<'.$to_email.'>',
		'subject'=>$subject,
		'html'=>$message,
		'text'=>strip_tags($message),
//		'o:tracking'=>'yes',
//		'o:tracking-clicks'=>'yes',
//		'o:tracking-opens'=>'yes',
//		'o:tag'=>$tag,
		'h:Reply-To'=>$sender_email
    );
    $session = curl_init(MAILGUN_URL.'/messages');
    curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  	curl_setopt($session, CURLOPT_USERPWD, 'api:'.MAILGUN_KEY);
    curl_setopt($session, CURLOPT_POST, true);
    curl_setopt($session, CURLOPT_POSTFIELDS, $array_data);
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_ENCODING, 'UTF-8');
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($session);
    curl_close($session);
    $results = json_decode($response, true);
	
	if ($results['message'] !== 'Queued. Thank you.'){
		return false;
	}
	
    return $results;

 
}


$current_backup = file_get_contents('./count.txt');

if (MAX_BACKUP_ALERT >= $current_backuo){
    $send = send_alert();
    if ($send){
    $fp = fopen('count.txt', 'w');
    fwrite($fp, '0');
    fclose($fp);
    } else {
    return false;
    }
}



